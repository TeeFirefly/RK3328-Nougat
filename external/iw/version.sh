#!/bin/sh

VERSION="4.1"
OUT="$1"

v="$VERSION"

echo '#include "iw.h"' > "$OUT"
echo "const char iw_version[] = \"$v\";" >> "$OUT"
