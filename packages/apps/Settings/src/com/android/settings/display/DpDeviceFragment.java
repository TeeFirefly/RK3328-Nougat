package com.android.settings.display;

public class DpDeviceFragment extends DeviceFragment {
    @Override
    protected DisplayInfo getDisplayInfo() {
        return DrmDisplaySetting.getDpDisplayInfo();
    }
}
