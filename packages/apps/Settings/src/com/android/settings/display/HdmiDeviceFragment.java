package com.android.settings.display;

public class HdmiDeviceFragment extends DeviceFragment {
    @Override
    protected DisplayInfo getDisplayInfo() {
        return DrmDisplaySetting.getHdmiDisplayInfo();
    }
}
